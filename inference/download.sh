#!/bin/bash

cur_path=`pwd`

if [ -f "$cur_path/model/chatglm.om" ]; then
    echo "[cur_path/model/chatglm.om] 已存在 "
else
    cd $cur_path/model
    wget https://obs-9be7.obs.cn-east-2.myhuaweicloud.com/wanzutao/chatglm3/chatglm.om
    echo "chatglm.om下载完成"
fi

if [ -f "$cur_path/tokenizer/tokenizer.zip" ]; then
    echo "[cur_path/tokenizer/tokenizer.zip] 已存在 "
else
    cd $cur_path/tokenizer
    wget https://obs-9be7.obs.cn-east-2.myhuaweicloud.com/wanzutao/chatglm3/tokenizer.zip
    unzip tokenizer.zip
    echo "tokenizer文件解压完成"
fi

cd $cur_path
pip3 install -r requirements.txt -i https://mirrors.huaweicloud.com/repository/pypi/simple
