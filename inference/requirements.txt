filelock==3.13.1
Flask==3.0.3
Flask-Cors==4.0.0
numpy==1.22.4
requests==2.31.0
sentencepiece==0.2.0
transformers==4.30.2
