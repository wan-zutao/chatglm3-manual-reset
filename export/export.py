import os
import sys
from typing import List
import torch
import transformers
import torch_npu
from transformers import AutoTokenizer, AutoModel
import torch_npu
import onnx
import io
def export_onnx(
    base_model: str = "models/OriginModel",
    output_dir: str = "models/OriginOnnx/chatglm.onnx",
):
    device = torch.device('npu:0')
    model = AutoModel.from_pretrained(
        base_model,
        torch_dtype=torch.float16
    ).to(device)
    quantize_cfg = {
        "query_key_value":{
            "type":"W8X8",
			"act_scale":False            
        },
        "dense":{
            "type":"W8X8",
			"act_scale":False
        },
        "dense_h_to_4h":{
			"type":"W8X8",
			"act_scale":False
		},
        "dense_4h_to_h":{
			"type":"W8X8",
			"act_scale":False
		}
	}
    '''

        

    '''
    input_names = ["input_ids", "position_ids","attention_mask" ,"past_key_values"]
    output_names = ["logits","out_key_values"]
    dynamic_axes = {
        "input_ids": { 0: "batch_size", 1: "seq_length" },
        "position_ids": { 0: "batch_size", 1: "seq_length" },
        "attention_mask": { 0: "batch_size",1:"all_len" },
        "past_key_values": { 2: "batch_size", 4: "kv_len" },
    }
    
    batch_size,seq_len,kv_len=1,1,1024
    all_len = seq_len + kv_len
    
    input_ids = torch.zeros((batch_size,seq_len)).long().to("npu") # batch_size, new_sequence_length
    attention_mask = torch.zeros((batch_size,all_len)).long().to("npu") # batch_size, all_sequence_length
    position_ids = torch.zeros((batch_size,seq_len)).long().to("npu") # batch_size, new_sequence_length
    past_key_values = torch.rand((28,2,kv_len, 1, 2, 128),dtype=torch.float16).to("npu")
    input_args = (
        input_ids,
        position_ids,
        attention_mask,
        past_key_values,
        None, # inputs_embeds: Optional[torch.FloatTensor] = None,
        None, #labels: Optional[torch.LongTensor] = None,
        True, #use_cache: Optional[bool] = None,
        True # output_attentions: Optional[bool] = None,
    )
    model.eval()
    with torch.no_grad():
        from quantize import quantize
        quantize(model,cfg=quantize_cfg)
        print(model)
        torch.onnx.export(
            model,
            f=output_dir,
            args=input_args,
            input_names=input_names,
            output_names=output_names,
            #dynamic_axes=dynamic_axes,
            do_constant_folding=False,
            opset_version=14,
            export_params=True  
        )


if __name__ == "__main__":
    export_onnx()
